# README #

### What is this repository for? ###

* This repo contains LiME (https://github.com/504ensicsLabs/LiME) .ko files and [Volatility] (https://github.com/volatilityfoundation/volatility) profiles for all official CentOS 5, 6, 7, 8 and Ubuntu 14.04 LTS, 16.04 LTS and 18.04 LTS kernels 
* Feel free to download the ones you need to your DFIR tasks
* Version 0.1

### Project status ###
* Currently up to Centos 8 and Ubuntu 18.04 LTS (linux-image-hwe branch not implemented yet)
* Upcoming Centos/RHEL and Ubuntu profiles are automatically generated and uploaded to this repository 

### Who do I talk to? ###

* contacto@securizame.com
* https://www.twitter.com/securizame
* https://www.securizame.com/contacto
